<%@ page import="com.org.fbi.entity.User" %>
<%@ page import="java.util.List" %>
<%@ page import="com.org.fbi.entity.CasesFbi" %>
<%@ page import="org.springframework.web.servlet.ModelAndView" %>
<%@ page import="com.org.fbi.entity.FbiDetails" %><%--
  Created by IntelliJ IDEA.
  User: Aditya Kumar
  Date: 9/24/2020
  Time: 3:19 PM
  To change this template use File | Settings | File Templates.
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link href='https://fonts.googleapis.com/css?family=Rowdies:300,regular,700' rel='stylesheet' />

    <title>Welcome</title>
    <style>
        body{
            color: aliceblue;
            font-family: 'Rowdies',sans-serif;
            background-color: azure;
            text-align: center;
        }
        .img img{
            text-align: center;
            border-radius: 50%;
        }
        .signup-form{
            width: auto;
            margin: 30px auto;
            color: rgb(255, 255, 255);
            text-shadow: 2px 2px 4px rgb(22, 17, 17);
            border-radius: 3px;
            margin-bottom: 15px;
            background:#9993;
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.6);
            padding: 30px;
        }
    </style>
</head>
<body>
<%
    User user = (User) request.getSession().getAttribute("user");
    int totalCase = 0;
    try {
        totalCase = (int) request.getAttribute("totalCase");
    } catch (Exception e) {
        System.out.println("null is running");
    }
    List<FbiDetails> fbidetails = (List<FbiDetails>) request.getSession().getAttribute("fbidetails");
    int noCases = (int) request.getSession().getAttribute("TotalNumberCasesByUser");
%>
<p style="color: #ffc107">Hey! Welcome Federal Bureau of Investigation</p>
<div class='img'>
    <img src='pics/springimg.jpg' alt='My photo' width='150px' height='150px'>
</div>
<h1 style="color: #ffc107">FBI Details:</h1>
<hr>

<div class='signup-form'>
    <p>Agent Name: <%= user.getFirst_name()+" "+user.getLast_name()%></p>
            <div clas="container">
                <form action="FbiDetailsInput.jsp">
                    <input type="hidden" value="<%=totalCase%>" name="casestotal">
                    <button type="submit" class="btn-success">Add Your Details</button>
                </form>
                <table class="table table-dark">
                    <thead>
                    <tr>
                        <th scope="col">FBI Id</th>
                        <th scope="col">FBI Location</th>
                        <th scope="col">FBI Code</th>
                        <th scope="col">FBI Office</th>
                        <th scope="col">Total Staffs</th>
                        <th scope="col">Total Officer</th>
                        <th scope="col">Total Cases</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:if test="${fbidetails!=null}">
                    <c:forEach items="${fbidetails}" var="fbid">
                    <tr>
                        <th scope="row">${fbid.id}</th>
                        <td>${fbid.location}</td>
                        <td>${fbid.fbiCode}</td>
                        <td>${fbid.officeName}</td>
                        <td>${fbid.staffCounts}</td>
                        <td>${fbid.officersCount}</td>
                        <td><%=noCases%></td>
                        <td>
                            <form action="UpdateFbiDetails.jsp">
                                <input type="hidden" name="id" value="${fbid.id}">
                                <input type="hidden" name="location" value="${fbid.location}">
                                <input type="hidden" name="fbiCode" value="${fbid.fbiCode}">
                                <input type="hidden" name="officeName" value="${fbid.officeName}">
                                <input type="hidden" name="staffCounts" value="${fbid.staffCounts}">
                                <input type="hidden" name="officersCount" value="${fbid.officersCount}">
                                <button type="submit" class="btn-success">Update</button>
                            </form>
                        </td>
                    </tr>
                    </c:forEach>
                    </c:if>
                    </tbody>
                </table>
                <br/>
                <h2 align="center">Cases Details</h2>
                <br/>
                <form action="FbiCasesInput.jsp">
                    <button type="submit" class="btn-success">Add Your Cases</button>
                </form>
            <table class="table table-striped table-dark">
            <thead>
            <tr>
            <th scope="col">Case Id</th>
            <td scope="col">Case Code</td>
            <td scope="col">Case Name</td>
            <td scope="col">Case Description</td>
            <td scope="col">Case Completed</td>
            <td scope="col">Case Officer</td>
            <td scope="col">Case OpenDate</td>
            <td scope="col">Case CloseDate</td>
            </tr>
            </thead>
            <tbody>
            <c:if test="${cases!=null}">
            <c:forEach items="${cases}" var="caseFbi">
            <tr>
            <th scope="row">${caseFbi.id}</th>
            <td>${caseFbi.caseName}</td>
            <td>${caseFbi.caseCode}</td>
            <td>${caseFbi.caseDescription}</td>
            <td>${caseFbi.caseCompleted}</td>
            <td>${caseFbi.caseHandleOfficerName}</td>
            <td>${caseFbi.caseOpenDate}</td>
            <td>${caseFbi.caseCloseDate}</td>
                <td>
                    <form action="UpdateCase.jsp">
                        <input type="hidden" name="id" value="${caseFbi.id}">
                        <input type="hidden" name="caseName" value="${caseFbi.caseName}">
                        <input type="hidden" name="caseCode" value="${caseFbi.caseCode}">
                        <input type="hidden" name="caseDescription" value="${caseFbi.caseDescription}">
                        <input type="hidden" name="caseOpenDate" value="${caseFbi.caseOpenDate}">
                        <input type="hidden" name="caseCloseDate" value="${caseFbi.caseCloseDate}">
                        <button type="submit" class="btn-success">Update</button>
                    </form>
                </td>
            </tr>
            </c:forEach>
            </c:if>
            </tbody>
            </table>
            </div>
</div>
</body>
</html>
