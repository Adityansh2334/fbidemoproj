<%@ page import="com.org.fbi.entity.User" %><%--
  Created by IntelliJ IDEA.
  User: Aditya Kumar
  Date: 9/24/2020
  Time: 4:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <title>Update Case</title>
</head>
<body>
<%
    User user = (User) request.getSession().getAttribute("user");
    String offNm=user.getFirst_name()+" "+user.getLast_name();
    String id = request.getParameter("id");
    String caseName = request.getParameter("caseName");
    String caseCode = request.getParameter("caseCode");
    String caseDescription = request.getParameter("caseDescription");
    String caseOpenDate = request.getParameter("caseOpenDate");
    String caseCloseDate = request.getParameter("caseCloseDate");
%>
<div class="container">
    <h1 align="center">Federal Bureau of Investigation</h1>
    <p align="center"><i> Law enforcement agency </i></p>
    <h3>Your Case Details</h3>
    <form class="needs-validation" novalidate action="updatecase" method="POST">
        <input type="hidden" name="caseIdUpdate" value="<%=id%>">
        <div class="job-content">
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="validationCustom01">Case name</label>
                    <input type="text" class="form-control" id="validationCustom01" name="caseName"  value="<%= caseName%>" required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationCustom02">Case code</label>
                    <input type="text" class="form-control" id="validationCustom02" name="caseCode"  value="<%= caseCode%>"required>
                    <div class="valid-feedback">
                        Looks good!
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationCustomUsername">Case Handle Officer's name</label>
                    <div class="input-group">
                        <input type="hidden" name="caseHandleOfficerName" value="<%=offNm%>">
                        <input type="text" class="form-control" id="validationCustomUsername"  placeholder="<%=offNm%>"  aria-describedby="inputGroupPrepend" disabled>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom03">Open Date</label>
                    <input type="date" class="form-control" name="caseOpenDate"   id="validationCustom03" value="<%= caseOpenDate%>" required>
                    <div class="invalid-feedback">
                        Please provide a valid date.
                    </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="validationCustom04">Close Date</label>
                    <input type="date" class="form-control" name="caseCloseDate"   id="validationCustom04" value="<%= caseCloseDate%>" >
                    <div class="invalid-feedback">
                        Please provide a valid date.
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="validationCustom05">Case Desciption</label>
                    <input type="text" class="form-control" name="caseDescription"   id="validationCustom05" value="<%= caseDescription%>" required>
                    <div class="invalid-feedback">
                        Please provide a valid Description.
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox"  id="invalidCheck" required>
                    <label class="form-check-label" for="invalidCheck">
                        Above mentioned data is correct and true.
                    </label>
                    <div class="invalid-feedback">
                        You must agree before submitting.
                    </div>
                </div>
                <h4>Case Status</h4>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline1" name="caseCompleted" class="custom-control-input" value="yes" required>
                    <label class="custom-control-label" for="customRadioInline1">Completed</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline2" name="caseCompleted" class="custom-control-input" value="no">
                    <label class="custom-control-label" for="customRadioInline2">Pendding</label>
                </div>
            </div>
            <button class="btn btn-primary" type="submit">Submit Case</button>
        </div>
    </form>
</div>

<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
</body>
</html>
