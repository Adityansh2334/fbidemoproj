package com.org.fbi.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "fbi_details")
public class FbiDetails implements Serializable {
    private static final long serialVersionUID = 211205042240603703L;
    @Id
    @GenericGenerator(name = "gen_id", strategy ="increment" )
    @GeneratedValue(generator = "gen_id")
    @Column(name = "id")
    private Long id;
    @Column(name = "location")
    private String location;
    @Column(name = "officeName")
    private String officeName;
    @Column(name = "fbiCode")
    private String fbiCode;
    @Column(name = "staffCounts")
    private Long staffCounts;
    @Column(name = "officersCount")
    private Long officersCount;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;
    @Transient
    private String city;
    @Transient
    private String state;
    @Transient
    private String zip;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFbiCode() {
        return fbiCode;
    }

    public void setFbiCode(String fbiCode) {
        this.fbiCode = fbiCode;
    }

    public Long getStaffCounts() {
        return staffCounts;
    }

    public void setStaffCounts(Long staffCounts) {
        this.staffCounts = staffCounts;
    }

    public Long getOfficersCount() {
        return officersCount;
    }

    public void setOfficersCount(Long officersCount) {
        this.officersCount = officersCount;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    @Override
    public String toString() {
        return "FbiDetails{" +
                "id=" + id +
                ", location='" + location + '\'' +
                ", officeName='" + officeName + '\'' +
                ", fbiCode='" + fbiCode + '\'' +
                ", staffCounts=" + staffCounts +
                ", officersCount=" + officersCount +
                ", user=" + user +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", zip='" + zip + '\'' +
                '}';
    }
}
