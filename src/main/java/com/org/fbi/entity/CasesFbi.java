package com.org.fbi.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cases_details")
public class CasesFbi implements Serializable {
    @Id
    @GenericGenerator(name = "gen_id", strategy ="increment" )
    @GeneratedValue(generator = "gen_id")
    @Column(name = "id")
    private Long id;
    @Column(name = "caseName")
    private String caseName;
    @Column(name = "caseCode")
    private String caseCode;
    @Column(name = "caseDescription")
    private String caseDescription;
    @Column(name = "caseCompleted")
    private Boolean caseCompleted;
    @Column(name = "caseHandleOfficerName")
    private String caseHandleOfficerName;
    @Column(name = "caseOpenDate")
    private String caseOpenDate;
    @Column(name = "caseCloseDate")
    private String caseCloseDate;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fbi_id")
    private FbiDetails fbiDetails;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaseName() {
        return caseName;
    }

    public void setCaseName(String caseName) {
        this.caseName = caseName;
    }

    public String getCaseCode() {
        return caseCode;
    }

    public void setCaseCode(String caseCode) {
        this.caseCode = caseCode;
    }

    public String getCaseDescription() {
        return caseDescription;
    }

    public void setCaseDescription(String caseDescription) {
        this.caseDescription = caseDescription;
    }

    public Boolean getCaseCompleted() {
        return caseCompleted;
    }

    public void setCaseCompleted(Boolean caseCompleted) {
        this.caseCompleted = caseCompleted;
    }

    public String getCaseHandleOfficerName() {
        return caseHandleOfficerName;
    }

    public void setCaseHandleOfficerName(String caseHandleOfficerName) {
        this.caseHandleOfficerName = caseHandleOfficerName;
    }

    public String getCaseOpenDate() {
        return caseOpenDate;
    }

    public void setCaseOpenDate(String caseOpenDate) {
        this.caseOpenDate = caseOpenDate;
    }

    public String getCaseCloseDate() {
        return caseCloseDate;
    }

    public void setCaseCloseDate(String caseCloseDate) {
        this.caseCloseDate = caseCloseDate;
    }

    public FbiDetails getFbiDetails() {
        return fbiDetails;
    }

    public void setFbiDetails(FbiDetails fbiDetails) {
        this.fbiDetails = fbiDetails;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
