package com.org.fbi.controller;

import com.org.fbi.dto.LoginValidation;
import com.org.fbi.entity.CasesFbi;
import com.org.fbi.entity.FbiDetails;
import com.org.fbi.entity.User;
import com.org.fbi.service.FbiService;
import com.org.fbi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.util.List;

@Controller
@RequestMapping("/")
public class LoginController {
    @Autowired
    private FbiService fbiService;
    @Autowired
    private UserService userService;
    @GetMapping(value = "/usersignin")
    public ModelAndView loginWork(LoginValidation validation,HttpServletRequest request){
        User user = userService.getuserDetailsBymailnPswd(validation.getEmail(), validation.getPassword());
        if(user!=null){
            request.getSession().setAttribute("TotalNumberCasesByUser",fbiService.getTotalCasesNumberByUserId(user.getId()));
            List<FbiDetails> fbiId = fbiService.getDetailsFbiByUserId(user.getId());
            request.getSession().setAttribute("fbidetails",fbiId);
            List<CasesFbi> casesFbis = fbiService.getFbiCasesDetailsByUserId(user.getId());
            FileOutputStream fos=null;
            try {
                Blob image = user.getImage();
                byte[] bytes = image.getBytes(1, (int)image.length());
                fos=new FileOutputStream("C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\FBIDemo\\pics\\springimg.jpg");
                fos.write(bytes);
                fos.flush();
                System.out.println("Write completed");
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                try {
                    fos.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            int totalCase=0;
            if(casesFbis!=null)totalCase=casesFbis.size();
            request.setAttribute("user",user);
            HttpSession session = request.getSession();
            session.setAttribute("user",user);
            request.setAttribute("totalCase",totalCase);
            return new ModelAndView("WelcomeFBI.jsp","cases",casesFbis);
        }
        return new ModelAndView("Login.jsp","name","Invalid Credentials! Please Try Again!");
    }
    @PostMapping("/fbidetails")
    public ModelAndView saveFbiDetails(FbiDetails fbiDetails, HttpServletRequest request){
        request.getSession().setAttribute("fbiDetail",fbiDetails);
        fbiDetails.setLocation(fbiDetails.getCity()+","+ fbiDetails.getState()+","+fbiDetails.getZip());
        User user = (User) request.getSession().getAttribute("user");
        request.setAttribute("userobj",user);
        fbiDetails.setUser(user);
        fbiService.savefbiDetails(fbiDetails);
        return new ModelAndView("FbiCasesInput.jsp","fbidetailsmsg","Your Details Saved!");
    }
}
