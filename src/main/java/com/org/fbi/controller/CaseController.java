package com.org.fbi.controller;

import com.org.fbi.entity.CasesFbi;
import com.org.fbi.entity.FbiDetails;
import com.org.fbi.entity.User;
import com.org.fbi.service.FbiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/")
public class CaseController {
    @Autowired
    private FbiService fbiService;
    @PostMapping("/casesdata")
    public ModelAndView saveCasesDetailByUser(CasesFbi fbi, HttpServletRequest request){
        FbiDetails fbidetails = (FbiDetails) request.getSession().getAttribute("fbiDetail");
        User user = (User) request.getSession().getAttribute("user");
        request.setAttribute("user",user);
        fbi.setFbiDetails(fbidetails);
        fbi.setUser(user);
        fbiService.saveCaseDetails(fbi);
        List<FbiDetails> fbiId = fbiService.getDetailsFbiByUserId(user.getId());
        request.getSession().setAttribute("fbidetails",fbiId);
        List<CasesFbi> caseId = fbiService.getFbiCasesDetailsByUserId(user.getId());
        request.setAttribute("totalCase",caseId.size());
        return new ModelAndView("WelcomeFBI.jsp","cases",caseId);
    }
}
