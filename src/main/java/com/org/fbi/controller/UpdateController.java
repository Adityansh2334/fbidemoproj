package com.org.fbi.controller;

import com.org.fbi.entity.CasesFbi;
import com.org.fbi.entity.FbiDetails;
import com.org.fbi.entity.User;
import com.org.fbi.service.FbiService;
import com.org.fbi.service.UpdateServiceFbi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/")
public class UpdateController {
    @Autowired
    private UpdateServiceFbi updateServiceFbi;
    @Autowired
    private FbiService fbiService;
    @PostMapping("/updatefbi")
    public ModelAndView updateFbiDetail(FbiDetails fbiDetails, HttpServletRequest request){
        FbiDetails fbiDetails1 = fbiService.getFbiDetails(Long.parseLong(request.getParameter("id")));
        fbiDetails1.setLocation(fbiDetails.getCity()+","+fbiDetails.getState()+","+fbiDetails.getZip());
        User user = (User) request.getSession().getAttribute("user");
        fbiDetails1.setUser(user);
        fbiDetails1.setFbiCode(fbiDetails.getFbiCode());
        fbiDetails1.setOfficeName(fbiDetails.getOfficeName());
        fbiDetails1.setOfficersCount(fbiDetails.getOfficersCount());
        fbiDetails1.setStaffCounts(fbiDetails.getStaffCounts());
        updateServiceFbi.updateFbiDetails(fbiDetails1);
        List<FbiDetails> fbiId = fbiService.getDetailsFbiByUserId(user.getId());
        request.getSession().setAttribute("fbidetails",fbiId);
        List<CasesFbi> casesList = fbiService.getFbiCasesDetailsByUserId(user.getId());
        return new ModelAndView("WelcomeFBI.jsp","cases",casesList);
    }
    @PostMapping("/updatecase")
    public ModelAndView updateCasesFbi(CasesFbi casesFbi, HttpServletRequest request){
        CasesFbi cases = fbiService.getCases(Long.parseLong(request.getParameter("caseIdUpdate")));
        User user = (User) request.getSession().getAttribute("user");
        cases.setUser(user);
        cases.setCaseCloseDate(casesFbi.getCaseCloseDate());
        cases.setCaseCode(casesFbi.getCaseCode());
        cases.setCaseCompleted(casesFbi.getCaseCompleted());
        cases.setCaseOpenDate(casesFbi.getCaseOpenDate());
        cases.setCaseDescription(casesFbi.getCaseDescription());
        cases.setCaseHandleOfficerName(casesFbi.getCaseHandleOfficerName());
        cases.setCaseName(casesFbi.getCaseName());
        cases.setFbiDetails((FbiDetails) request.getSession().getAttribute("fbiDetail"));
        updateServiceFbi.updateFbiCases(cases);
        List<FbiDetails> fbiId = fbiService.getDetailsFbiByUserId(user.getId());
        request.getSession().setAttribute("fbidetails",fbiId);
        List<CasesFbi> casesList = fbiService.getFbiCasesDetailsByUserId(user.getId());
        return new ModelAndView("WelcomeFBI.jsp","cases",casesList);
    }
}
