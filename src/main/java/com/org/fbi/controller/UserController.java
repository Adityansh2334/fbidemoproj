package com.org.fbi.controller;

import com.org.fbi.entity.FbiDetails;
import com.org.fbi.entity.User;
import com.org.fbi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/")
public class UserController {
        @Autowired
        private UserService userService;
        @PostMapping(value = "/usersignup")
        public ModelAndView sigupWork(User user, HttpServletRequest request){
            request.getSession().setAttribute("user",user);
            userService.saveUserDetails(user);
            return new ModelAndView("Login.jsp","name","Registration " +
                    "Successful! Please Login");
        }

}
