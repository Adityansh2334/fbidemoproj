package com.org.fbi.dao;

import com.org.fbi.entity.CasesFbi;
import com.org.fbi.entity.FbiDetails;
import com.org.fbi.entity.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FbiDao {
    @Autowired
    private SessionFactory factory;
    Session session = null;
    public void saveFbiDetails(FbiDetails fbi) {
        try {
            session = factory.openSession();
            Transaction transaction = session.beginTransaction();
            session.save(fbi);
            transaction.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
    }
    public void saveCaseDetails(CasesFbi fbi){
        try {
            session = factory.openSession();
            Transaction transaction = session.beginTransaction();
            session.save(fbi);
            transaction.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
    }
    @SuppressWarnings("unchecked")
    public List<FbiDetails> getFbiDetailsByUserId(Long id){
        List<FbiDetails> fbiDetails=null;
        try {
            Session session = factory.openSession();
            String hql="from "+FbiDetails.class.getName()+" where user.id=:id";
            Query<FbiDetails> query = session.createQuery(hql);
            query.setParameter("id",id);
            fbiDetails=query.list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return fbiDetails;
    }
    @SuppressWarnings("unchecked")
    public List<CasesFbi> getFbiCasesDetailsByUserId(Long id){
        List<CasesFbi> casesFbi=null;
        try {
            Session session = factory.openSession();
            String hql="from "+CasesFbi.class.getName()+" where user.id=:id";
            Query<CasesFbi> query = session.createQuery(hql);
            query.setParameter("id",id);
            casesFbi= query.list();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return casesFbi;
    }
    public int getTotalCasesByUserid(Long id){
        try {
            Session session = factory.openSession();
            String hql="from "+CasesFbi.class.getName()+" where user.id=:id";
            Query<CasesFbi> query = session.createQuery(hql);
            query.setParameter("id",id);
            return query.list().size();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public FbiDetails getFbiDetailsByFbiId(Long id){
        Session session = factory.openSession();
        return  session.get(FbiDetails.class,id);
    }
    public CasesFbi getCasesByCaseId(Long id){
        Session session = factory.openSession();
        return session.get(CasesFbi.class,id);
    }
}
