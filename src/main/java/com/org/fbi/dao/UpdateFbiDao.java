package com.org.fbi.dao;

import com.org.fbi.entity.CasesFbi;
import com.org.fbi.entity.FbiDetails;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UpdateFbiDao {
        @Autowired
        private SessionFactory sessionFactory;

        public void updateFbiDetailsById(FbiDetails fbiDetails){
            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            session.update(fbiDetails);
            transaction.commit();;
        }
        public void updateFbiCasesById(CasesFbi casesFbi){
            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            session.update(casesFbi);
            transaction.commit();;
        }
}
