package com.org.fbi.dao;

import com.org.fbi.entity.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.Blob;
import java.sql.SQLException;

@Repository
public class UserDao {
    @Autowired
    private SessionFactory factory;
    Session session=null;
    public void saveUserFBIDetails(User user){
        try {
            session = factory.openSession();
            Transaction transaction = session.beginTransaction();
            CommonsMultipartFile img = user.getImg();
            byte[] bytes = img.getBytes();
            Blob blob = new SerialBlob(bytes);
            user.setImage(blob);
            session.save(user);
            transaction.commit();
        } catch (HibernateException | SQLException e) {
            e.printStackTrace();
        } finally {
            if (session == null) throw new AssertionError();
            session.close();
        }
    }
    @SuppressWarnings("unchecked")
    public User getUserDetailsByEmailnPswd(String email, String password){
        User user=null;
        try {
            session = factory.openSession();
            String hql="from "+User.class.getName()+" where email=:mail and password=:pass";
            Query<User> query = session.createQuery(hql);
            query.setParameter("mail",email).setParameter("pass",password);
            user=query.uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return user;
    }

}
