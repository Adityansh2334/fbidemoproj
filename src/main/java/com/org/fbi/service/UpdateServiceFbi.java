package com.org.fbi.service;

import com.org.fbi.dao.UpdateFbiDao;
import com.org.fbi.entity.CasesFbi;
import com.org.fbi.entity.FbiDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateServiceFbi {
    @Autowired
    private UpdateFbiDao updateFbiDao;

    public void updateFbiDetails(FbiDetails fbiDetails){
        updateFbiDao.updateFbiDetailsById(fbiDetails);
    }
    public void updateFbiCases(CasesFbi casesFbi){
        updateFbiDao.updateFbiCasesById(casesFbi);
    }
}
