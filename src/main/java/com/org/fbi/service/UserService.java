package com.org.fbi.service;

import com.org.fbi.dao.UserDao;
import com.org.fbi.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserDao userDao;

    public void saveUserDetails(User user){
        userDao.saveUserFBIDetails(user);
    }
    public User getuserDetailsBymailnPswd(String mail,String pasword){
        return userDao.getUserDetailsByEmailnPswd(mail,pasword);
    }
}
