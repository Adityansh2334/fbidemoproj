package com.org.fbi.service;

import com.org.fbi.dao.FbiDao;
import com.org.fbi.entity.CasesFbi;
import com.org.fbi.entity.FbiDetails;
import com.org.fbi.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FbiService {
    @Autowired
   private FbiDao fbiDao;
    public void savefbiDetails(FbiDetails fbiDetails){
        fbiDao.saveFbiDetails(fbiDetails);
    }
    public void saveCaseDetails(CasesFbi fbi){
        fbiDao.saveCaseDetails(fbi);
    }
    public List<FbiDetails> getDetailsFbiByUserId(Long id){
       return fbiDao.getFbiDetailsByUserId(id);
    }
    public List<CasesFbi> getFbiCasesDetailsByUserId(Long id){
        return fbiDao.getFbiCasesDetailsByUserId(id);
    }
    public int getTotalCasesNumberByUserId(Long id){
        return fbiDao.getTotalCasesByUserid(id);
    }
    public FbiDetails getFbiDetails(Long id){
       return fbiDao.getFbiDetailsByFbiId(id);
    }
    public CasesFbi getCases(Long id){
        return  fbiDao.getCasesByCaseId(id);
    }
}
