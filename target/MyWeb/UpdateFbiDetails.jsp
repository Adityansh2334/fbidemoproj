<%--
  Created by IntelliJ IDEA.
  User: Aditya Kumar
  Date: 9/24/2020
  Time: 2:39 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <title>Update FBI Details</title>
</head>
<body>
<%
    String id = request.getParameter("id");
    String location = request.getParameter("location");
    String[] locations = location.split(",");
    String city = null,state = null,pin=null;
    if(locations.length>=3) {
        city = locations[0];
        state = locations[1];
        pin = locations[2];
    }
    String fbiCode = request.getParameter("fbiCode");
    String officeName = request.getParameter("officeName");
    String staffCounts = request.getParameter("staffCounts");
    String officersCount = request.getParameter("officersCount");

%>
<div class="container">
    <h1 align="center">Federal Bureau of Investigation</h1>
    <p align="center"><i> Law enforcement agency </i></p>
    <br>
    <h1 align="center">Fill-up your Details </h1>
    <form action="updatefbi" method="POST">
        <input type="hidden" name="id" value="<%= id%>">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Office Name</label>
                <input type="text" class="form-control" id="inputEmail4" name="officeName" value="<%= officeName%>">
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">FBI Code</label>
                <input type="text" class="form-control" id="inputPassword4" name="fbiCode"  value="<%= fbiCode%>">
            </div>
        </div>
        <div class="form-group">
            <label for="inputAddress">Total Staffs</label>
            <input type="number" class="form-control" id="inputAddress" name="staffCounts"  value="<%= staffCounts%>">
        </div>
        <div class="form-group">
            <label for="inputAddress2">Total Officers</label>
            <input type="number" class="form-control" id="inputAddress2" name="officersCount"  value="<%= officersCount%>">
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputCity">City</label>
                <input type="text" class="form-control" name="city" value="<%= city%>" id="inputCity">
            </div>
            <div class="form-group col-md-4">
                <label for="inputState">State</label>
                <input type="text" class="form-control" name="state"  value="<%= state%>" id="inputState">
            </div>
            <div class="form-group col-md-2">
                <label for="inputZip">Zip</label>
                <input type="text" name="zip"  class="form-control" value="<%= pin%>" id="inputZip">
            </div>
        </div>
        <div class="form-group">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="gridCheck">
                <label class="form-check-label" for="gridCheck">
                    Above details are correct and true.
                </label>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit Details</button>
        <p>*Note* This page will directly redirect into "Cases Details Entry" page.</p>
    </form>
</div>
</body>
</html>
